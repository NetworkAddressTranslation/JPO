# Concepts de base

## Composition d'une commande

Une ligne de commande est faite d'une commande, d'options et d'arguments.

Par exemple :

`ls -l /home/nathan`

*ls* est la commande, *-l* est une option et */home/nathan* est un argument.
Il peut y avoir aucune option et aucun argument, ou autant d'options et d'arguments que l'on veut.

## L'arborescence dans Linux

![](tree.png)

Dans un système Linux, les chemins sont représentés sous la forme */repertoire1/repertoire2/fichier1*, où les répertoires sont séparés par des barres obliques.

Lorsque l'on utilise un interpréteur en ligne de commandes Linux, nous nous trouvons toujours dans un répertoire, le *répertoire courant*.
Le répertoire qui contient le répertoire courant se nomme *répertoire parent*.
Au démarrage de cet interpréteur, nous sommes dans le *répertoire utilisateur*, qui a pour répertoire parent */home*.

Certains répertoires ont des chemins particuliers :

* la racine du système : */*,
* le répertoire utilisateur : *~*, qui correspond à */home/utilisateur*,
* le répertoire courant : *./* ou un espace vide,
* le répertoire parent : *../*.

Tout chemin commence par l'un de ces chemins. Un chemin est *relatif* (au répertoire courant) s'il commence par le *répertoire courant* ou *parent*, ou ce chemin est *absolu* s'il part de la *racine* ou du *répertoire utilisateur* (il ne dépend pas du répertoire courant).

## Raccourcis

![](tab.jpg)

Raccourci    | Action
-------------|----------------------------------------
TAB          | Autocomplète la commande ou le chemin actuel
Ctrl + A     | Déplace le curseur en début de commande
Ctrl + E     | Déplace le curseur en fin de commande
Ctrl + C     | Force l'arrêt de la commande en cours
Ctrl + D     | Ferme l'interpréteur courant (équivalent à la commande `exit`)
Ctrl + L     | Efface le contenu du terminal (équivalent à la commande `clear`)
Ctrl + R     | Permet de recherche une commande dans l'historique
Clic central | Colle le contenu du presse-papier

# Commandes bash

## Commandes de base

### adduser

![](adduser.png)

`adduser` ajoute des utilisateurs au système.

Exemple :

`adduser nathan eleves` créée l'utilisateur *nathan* et l'ajoute au groupe *eleves*.

### apt-get

![](apt-get.png)

`apt-get` permet de gérer les paquets d'un système Linux.

Exemples :

* `apt-get install firefox` installe le paquet de Firefox.
* `apt-get remove firefox` désinstalle le paquet de Firefox.
* `apt-get upgrade` met à jour le système.

### touch

![](touch.png)

`touch` créée un fichier ou change l'heure d'accès et de modification d'un fichier existant.

Exemples :

* `touch fichier.txt` créée le fichier *fichier.txt*.
* `touch fichier.txt -d "2019-04-13 16:13:23"` changera le temps d'accès et de modification du fichier *fichier.txt* au samedi 13 Avril 2019 à 16h13.

### echo

![](echo.png)

`echo` affiche une ligne de texte dans le terminal.

Exemple :

`echo "Bonjour !"` affiche "Bonjour !" dans le terminal
